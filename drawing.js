// Copyright (C) 2020 by Mattia Monga <mattia.monga@unimi.it>
// Basic idea from https://gist.github.com/peshoicov/ab3286875d980948ad3f5f434fec37a9
// and http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html

Conticini.reset_canvases = function (just_this = null) {
    var drawing = null;
    var mousePos = {x:0, y:0};
    var lastPos = mousePos;
    var isMobile = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    const canvasDim = 0.9*(Conticini.CONTICINO.clientWidth / 2);
    Conticini.CIFRA_D.has_listeners = false;
    Conticini.CIFRA_U.has_listeners = false;

    if (just_this === null) {
        setup_canvas(Conticini.CIFRA_D);
        setup_canvas(Conticini.CIFRA_U);
    } else {
        setup_canvas(just_this);
        if (just_this === Conticini.CIFRA_D)
            Conticini.CIFRA_U.is_readonly = true;
        else
            Conticini.CIFRA_D.is_readonly = true;
    }

    Conticini.check_msg();

    function setup_canvas(canvas){
        // Setup canvas ..
        const ctx = canvas.getContext('2d');

        canvas.is_blank = true;    // blank is ok in cifra_d
        canvas.is_readonly = false; // don't draw on a readonly canvas!
        canvas.width = canvasDim;
        canvas.height = canvas.width;

        // setup lines styles ..
        //ctx.strokeStyle = "#22222";
        ctx.lineWidth = canvasDim / 28;
        ctx.lineCap = 'round';

        // mouse/touch events ..
        if (!canvas.has_listeners) {
            canvas.addEventListener((isMobile ? 'touchstart' : 'mousedown'), function(e) {
                if (!canvas.is_readonly){
                    drawing = ctx.canvas.id;
                    lastPos = get_mouse_pos(canvas, e);
                    mousePos = lastPos;
                }
            });
            canvas.addEventListener((isMobile ? 'touchmove' : 'mousemove'), function(e) {
                if (!canvas.is_readonly){
                    mousePos = get_mouse_pos(canvas, e);
                }
            });
            canvas.addEventListener((isMobile ? 'touchend' : 'mouseup'), function(e) {
                if (!canvas.is_readonly){
                    drawing = null;
                }
            });
            canvas.has_listeners = true;
        }

        // drawing ..
        window.requestAnimFrame = (function(callback) {
            return	window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function(callback) {
                    window.setTimeout(callback, 1000/60);
                };
        })();

        function draw_loop() {
            window.requestAnimFrame(draw_loop);
            render_canvas(ctx);
        }

        draw_loop();
    }

    // helper functions ..
    function get_mouse_pos(canvasDom, touchOrMouseEvent) {
        const rect = canvasDom.getBoundingClientRect();
        return {
            x: (isMobile ? touchOrMouseEvent.touches[0].clientX : touchOrMouseEvent.clientX) - rect.left,
            y: (isMobile ? touchOrMouseEvent.touches[0].clientY : touchOrMouseEvent.clientY) - rect.top
        };
    }

    function render_canvas(ctx) {
        if (drawing === ctx.canvas.id) {
            ctx.canvas.is_blank = false;
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            lastPos = mousePos;
        }
    }

};
