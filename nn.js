// Copyright (C) 2020 by Mattia Monga <mattia.monga@unimi.it>
// Code that consume ONNX.js mainly from https://github.com/microsoft/onnxjs-demo

Conticini.nn = {
    getNumber: async function(canvas, tmp) {
        const myOnnxSession = new onnx.InferenceSession();
        // load the ONNX model file
        await myOnnxSession.loadModel("./model.onnx");
        // Preprocess the image data to match input dimension requirement, which is 1*1*28*28.
        const width = 28;
        const height = 28;
        const preprocessedData = this.preprocess(canvas, tmp, width, height);
        const inputTensor = new onnx.Tensor(preprocessedData, 'float32', [1, 1, width, height]);

        const outputMap = await myOnnxSession.run([inputTensor]);
        const outputData = outputMap.values().next().value.data;
        const probabilities = this.softmax(outputData);
        console.log(canvas.id, Array.from(probabilities.map((x) => 100*x).entries()).toString());
        return this[Conticini.cfg.DECISION_RULE](probabilities); // -1 is not found
    },

    getMajority: function(probs) {
        return probs.findIndex((x) => x > .5);
    },

    getMax: function(probs, thresold = .4) {
        const candidates = probs.filter((x) => x > .4);
        return candidates.findIndex(Math.max(...candidates));
    },


    softmax: function(arr) {
        const C = Math.max(...arr);
        const d = arr.map((y) => Math.exp(y - C)).reduce((a, b) => a + b);
        return arr.map((value, index) => {
            return Math.exp(value - C) / d;
        });
    },

    preprocess: function(canvas, tmp, width, height) {
        const ctx = canvas.getContext('2d');
        const ctxScaled = tmp.getContext('2d');

        ctxScaled.save();
        ctxScaled.scale(width / ctx.canvas.width, height / ctx.canvas.height);
        ctxScaled.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctxScaled.drawImage(canvas, 0, 0);
        const imageDataScaled = ctxScaled.getImageData(0, 0, ctxScaled.canvas.width, ctxScaled.canvas.height);
        ctxScaled.restore();
        // process image data for model input
        const data = imageDataScaled.data;
        const input = new Float32Array(width * height);
        for (let i = 0; i < data.length; i += 4) {
        input[i / 4] = data[i + 3] / 255;
        }
        return input;
    },
};
