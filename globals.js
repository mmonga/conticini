// HTML elements

var Conticini = {

    CONTICINO: document.getElementById('operazione'),
    CIFRA_D: document.getElementById('cifra_d'),
    CIFRA_U: document.getElementById('cifra_u'),
    CANVAS_TMP:  document.getElementById('canvas_tmp'),
    OPERANDO_SX:  document.getElementById('operando_sx'),
    OPERANDO_DX: document.getElementById('operando_dx'),
    OPERATORE: document.getElementById('operatore'),
    PULSANTE: document.getElementById('pulsante'),

    // Feel free to configure these
    cfg: {
        MIN_OPERANDO: localStorage.getItem('MIN_OPERANDO') || 0,
        MAX_OPERANDO: localStorage.getItem('MAX_OPERANDO') || 9,
        MIN_EXPECTED: localStorage.getItem('MIN_EXPECTED') || 0,
        MAX_EXPECTED: localStorage.getItem('MAX_EXPECTED') || 20,
        OPS: [localStorage.getItem('DISABLE_PLUS') === 'true' ? null : '+',
              localStorage.getItem('DISABLE_MINUS') === 'true' ? null : '-',
              localStorage.getItem('ENABLE_TIMES') === 'true' ? '*' : null,
              localStorage.getItem('ENABLE_DIV') === 'true' ? '/' : null,
             ].filter((x) => x !== null),

        OK_TIMEOUT: localStorage.getItem('OK_TIMEOUT') || 5000,
        KO_TIMEOUT: localStorage.getItem('KO_TIMEOUT') || 3000,
        MSG_CHECK: localStorage.getItem('MSG_CHECK') || "CONTROLLA",
        MSG_OK: localStorage.getItem('MSG_OK') || "BENISSIMO!",
        MSG_KO: localStorage.getItem('MSG_KO') || "HAI SCRITTO MALE, RIPROVA!",
        DECISION_RULE: localStorage.getItem('MSG_KO') || "getMajority",
    },
};

