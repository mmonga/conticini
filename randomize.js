// Copyright (C) 2020 by Mattia Monga <mattia.monga@unimi.it>

Conticini.randomize = function() {
    var op_sx, op, op_dx;
    do {
        op_sx = Math.floor(Conticini.cfg.MIN_OPERANDO
                                   + Math.random()*(Conticini.cfg.MAX_OPERANDO - Conticini.cfg.MIN_OPERANDO + 1));
        op_dx = Math.floor(Conticini.cfg.MIN_OPERANDO
                                   + Math.random()*(Conticini.cfg.MAX_OPERANDO - Conticini.cfg.MIN_OPERANDO + 1));
        op = Conticini.cfg.OPS[Math.floor(Math.random()*Conticini.cfg.OPS.length)];
        Conticini.expected = Math.floor(eval(op_sx + op + op_dx));
        Conticini.expected_d = Math.floor(Conticini.expected / 10);
        Conticini.expected_u = Conticini.expected % 10;
    } while (Conticini.expected < Conticini.cfg.MIN_EXPECTED || Conticini.expected > Conticini.cfg.MAX_EXPECTED);

    Conticini.OPERANDO_SX.innerHTML = "" + op_sx;
    Conticini.OPERANDO_DX.innerHTML = "" + op_dx;
    if (op === '/')
        Conticini.OPERATORE.innerHTML = ':';
    else if (op === '*')
        Conticini.OPERATORE.innerHTML = '&times;';
    else
        Conticini.OPERATORE.innerHTML = op;
    Conticini.reset_canvases();
};

Conticini.check_msg = function() {
    Conticini.PULSANTE.value = Conticini.cfg.MSG_CHECK;
    Conticini.PULSANTE.className = 'check';
    Conticini.PULSANTE.onclick = Conticini.check_result;
};

Conticini.ko_msg = function(timeout, canvas = null) {
    Conticini.PULSANTE.value = Conticini.cfg.MSG_KO;
    Conticini.PULSANTE.className = 'ko';
    window.setTimeout(() => Conticini.reset_canvases(canvas), timeout);
};

Conticini.ok_msg = function(timeout) {
    Conticini.PULSANTE.value = Conticini.cfg.MSG_OK;
    Conticini.PULSANTE.className = 'ok';
    window.setTimeout(Conticini.randomize, timeout);
};


Conticini.check_result = async function() {
    const u = await Conticini.nn.getNumber(Conticini.CIFRA_U, Conticini.CANVAS_TMP);
    const d = await Conticini.nn.getNumber(Conticini.CIFRA_D, Conticini.CANVAS_TMP);
    if ((d === Conticini.expected_d
         || (Conticini.CIFRA_D.is_blank && Conticini.expected_d === 0))
        && u === Conticini.expected_u) {
        Conticini.ok_msg(Conticini.cfg.OK_TIMEOUT);
        return;
    }
    if (d === Conticini.expected_d && u !== Conticini.expected_u) {
        Conticini.ko_msg(Conticini.cfg.KO_TIMEOUT, Conticini.CIFRA_U);
        return;
    }
    if (d !== Conticini.expected_d && u === Conticini.expected_u) {
        Conticini.ko_msg(Conticini.cfg.KO_TIMEOUT, Conticini.CIFRA_D);
        return;
    }
    if ((d === -1 && u === Conticini.expected_u) || (d > -1 && d === Conticini.expected_d)) {
    }
    Conticini.ko_msg(Conticini.cfg.KO_TIMEOUT);
};
