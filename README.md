Un'applicazione molto semplice per esercitarsi con i conticini. Le cifre si
scrivono con il dito o il mouse e vengono riconosciute da una rete neurale
addestrata sul dataset MNIST (vedi
https://github.com/onnx/models/tree/master/vision/classification/mnist).

Questo programma è software libero (GPLv3) e può quindi essere usato, copiato,
distribuito, modificato a piacimento conservando gli stessi diritti ai nuovi
utenti.

Provalo qui: https://mmonga.gitlab.io/conticini/
